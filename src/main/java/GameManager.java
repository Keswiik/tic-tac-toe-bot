package main.java;

import java.sql.SQLException;
import java.util.Random;
import java.util.Scanner;

public class GameManager {
	private final boolean AUTO_PLAY;
	private int fGamesRemaining;
	private final TicTacToeBot[] PLAYERS;

	public GameManager(boolean autoPlay, int gamesRemaining) {
		AUTO_PLAY = autoPlay;
		fGamesRemaining = gamesRemaining;

		PLAYERS = new TicTacToeBot[2];
		for (int i = 0; i < PLAYERS.length; i++)
			PLAYERS[i] = new TicTacToeBot(MovePiece.values()[i]);
	}

	public GameManager(int gamesRemaining) {
		this(true, gamesRemaining);
	}

	public void play() throws SQLException {
		while (fGamesRemaining > 0) {
			Board board = new Board();
			int currentPlayer = new Random().nextInt(PLAYERS.length);
			while (!board.isFinished()) {
				PLAYERS[currentPlayer].makeMove(board.getAvailableMoves(), board.getBoardState());
				currentPlayer = (currentPlayer + 1) % PLAYERS.length;
			}

			MovePiece winner = board.getWinner();
			for (TicTacToeBot player : PLAYERS) {
				GameResult result = MovePiece.UNDEFINED.equals(winner) ? GameResult.TIE
						: winner.equals(player.getPiece()) ? GameResult.WIN : GameResult.LOSE;
				player.endGame(result);
			}

			if (!AUTO_PLAY && !showPlayPrompt())
				break;
		}
	}

	private boolean showPlayPrompt() {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Play another game (Y/N) > ");
		String input = scanner.nextLine();
		scanner.close();

		return input.equalsIgnoreCase("y");
	}
}
