package main.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreSQLJDBC {
	private static Connection DATABASE_CONNECTION = null;

	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					DATABASE_CONNECTION.close();
					System.out.println("Connection to database terminated");
				} catch (SQLException e) {
					System.err.println(e.getClass().getName() + ": " + e.getMessage());
				}
			}
		});
	}

	public static Connection getConnection() {
		if (DATABASE_CONNECTION == null)
			makeConnection();

		return DATABASE_CONNECTION;
	}

	public static void main(String[] args) {
		PostgreSQLJDBC.getConnection();
		System.exit(0);
	}

	private static void makeConnection() {
		try {
			DATABASE_CONNECTION = DriverManager.getConnection("jdbc:postgresql://localhost:5432/moves", "postgres",
					"THIS16letterword");
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Connection to database established");
	}
}
