package main.java;

public class Move {
	public final MovePiece fPiece;
	private final Integer fCol;
	private final Integer fRow;

	private Move(final Builder builder) {
		if (builder.fRow == null || builder.fRow > 2 || builder.fRow < 0)
			throw new IllegalArgumentException("Invalid row specified.");
		else if (builder.fCol == null || builder.fCol > 2 || builder.fCol < 0)
			throw new IllegalArgumentException("Invalid row specified");

		fRow = builder.fRow;
		fCol = builder.fCol;
		if (builder.fPiece == null)
			fPiece = MovePiece.UNDEFINED;
		else
			fPiece = builder.fPiece;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Move other = (Move) obj;
		if (fCol == null) {
			if (other.fCol != null)
				return false;
		} else if (!fCol.equals(other.fCol))
			return false;
		if (fPiece != other.fPiece)
			return false;
		if (fRow == null) {
			if (other.fRow != null)
				return false;
		} else if (!fRow.equals(other.fRow))
			return false;
		return true;
	}

	public int getColumn() {
		return fCol;
	}

	public MovePiece getPiece() {
		return fPiece;
	}

	public int getRow() {
		return fRow;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (fCol == null ? 0 : fCol.hashCode());
		result = prime * result + (fPiece == null ? 0 : fPiece.hashCode());
		result = prime * result + (fRow == null ? 0 : fRow.hashCode());
		return result;
	}

	public static final class Builder extends main.java.builders.Builder<Move> {
		public MovePiece fPiece;
		private Integer fRow, fCol;

		@Override
		public Move build() {
			setBuildingComplete();
			return new Move(this);
		}

		public Builder withColumn(final int col) {
			verifyBuildingState();
			fCol = col;
			return this;
		}

		public Builder withPiece(final MovePiece piece) {
			verifyBuildingState();
			fPiece = piece;
			return this;
		}

		public Builder withRow(final int row) {
			verifyBuildingState();
			fRow = row;
			return this;
		}

		public static Builder create() {
			return new Builder();
		}

		public static Builder create(final Move move) {
			Builder builder = new Builder();
			builder.fRow = move.fRow;
			builder.fCol = move.fCol;
			builder.fPiece = move.fPiece;
			return builder;
		}
	}
}
