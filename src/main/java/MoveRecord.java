package main.java;

public class MoveRecord {
	private final String fBoardState;

	private Integer fLosses;

	private final Move fMove;
	private Integer fTies;
	private Integer fWins;

	private MoveRecord(final Builder builder) {
		if (builder.fBoardState == null)
			throw new IllegalArgumentException("Board state cannot be null");
		else if (builder.fLosses == null)
			throw new IllegalArgumentException("Losses cannot be null");
		else if (builder.fTies == null)
			throw new IllegalArgumentException("Ties cannot be null");
		else if (builder.fWins == null)
			throw new IllegalArgumentException("Wins cannot be null");
		else if (builder.fMove == null)
			throw new IllegalArgumentException("Move cannot be null");

		fLosses = builder.fLosses;
		fTies = builder.fTies;
		fWins = builder.fWins;
		fMove = builder.fMove;
		fBoardState = builder.fBoardState;
	}

	public void addLoss() {
		fLosses++;
	}

	public void addTie() {
		fTies++;
	}

	public void addWin() {
		fWins++;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoveRecord other = (MoveRecord) obj;
		if (fBoardState == null) {
			if (other.fBoardState != null)
				return false;
		} else if (!fBoardState.equals(other.fBoardState))
			return false;
		if (fLosses == null) {
			if (other.fLosses != null)
				return false;
		} else if (!fLosses.equals(other.fLosses))
			return false;
		if (fMove == null) {
			if (other.fMove != null)
				return false;
		} else if (!fMove.equals(other.fMove))
			return false;
		if (fTies == null) {
			if (other.fTies != null)
				return false;
		} else if (!fTies.equals(other.fTies))
			return false;
		if (fWins == null) {
			if (other.fWins != null)
				return false;
		} else if (!fWins.equals(other.fWins))
			return false;
		return true;
	}

	public String getBoardState() {
		return fBoardState;
	}

	public Integer getLosses() {
		return fLosses;
	}

	public Move getMove() {
		return fMove;
	}

	public Integer getTies() {
		return fTies;
	}

	public Integer getWins() {
		return fWins;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (fBoardState == null ? 0 : fBoardState.hashCode());
		result = prime * result + (fLosses == null ? 0 : fLosses.hashCode());
		result = prime * result + (fMove == null ? 0 : fMove.hashCode());
		result = prime * result + (fTies == null ? 0 : fTies.hashCode());
		result = prime * result + (fWins == null ? 0 : fWins.hashCode());
		return result;
	}

	public static final class Builder extends main.java.builders.Builder<MoveRecord> {
		private String fBoardState;
		private Integer fLosses;
		private Move fMove;
		private Integer fTies;
		private Integer fWins;

		@Override
		public MoveRecord build() {
			setBuildingComplete();
			return new MoveRecord(this);
		}

		public Builder withBoardState(final String boardState) {
			verifyBuildingState();
			fBoardState = boardState;
			return this;
		}

		public Builder withLosses(final int losses) {
			verifyBuildingState();
			fLosses = losses;
			return this;
		}

		public Builder withMove(final Move move) {
			verifyBuildingState();
			fMove = move;
			return this;
		}

		public Builder withTies(final int ties) {
			verifyBuildingState();
			fTies = ties;
			return this;
		}

		public Builder withWins(final int wins) {
			verifyBuildingState();
			fWins = wins;
			return this;
		}

		public static Builder create() {
			return new Builder();
		}

		public static Builder create(MoveRecord moveRecord) {
			Builder builder = new Builder();
			builder.fBoardState = moveRecord.fBoardState;
			builder.fWins = moveRecord.fWins;
			builder.fTies = moveRecord.fTies;
			builder.fLosses = moveRecord.fLosses;
			builder.fMove = moveRecord.fMove;

			return builder;
		}
	}
}
