package main.java;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class TicTacToeBot {
	private Connection fCon;
	private MovePiece fMovePiece;
	private ArrayList<MoveRecord> fMovesMade;
	private HashMap<MoveRecord, Boolean> fNewMoveMap;

	public TicTacToeBot(MovePiece movePiece) {
		if (movePiece == null || movePiece.equals(MovePiece.UNDEFINED))
			throw new IllegalArgumentException("Cannot create a bot with no move piece");

		fCon = PostgreSQLJDBC.getConnection();

		fMovesMade = new ArrayList<>();
		fNewMoveMap = new HashMap<>();
	}

	public void endGame(GameResult result) throws SQLException {
		for (MoveRecord move : fMovesMade) {
			if (GameResult.WIN.equals(result))
				move.addWin();
			else if (GameResult.TIE.equals(result))
				move.addTie();
			else if (GameResult.LOSE.equals(result))
				move.addLoss();

			saveMove(move);
		}

		fMovesMade.clear();
	}

	public MovePiece getPiece() {
		return fMovePiece;
	}

	/**
	 * Chooses a move from a list of available moves.
	 *
	 * It will first attempt to pull information from the database.
	 *
	 * First, the highest-scoring {@link MoveRecord} will be found. If
	 * {@code <= 0}, then a {@link Move} will be chosen from all unknown,
	 * currently available moves.
	 *
	 * If all moves are known, it will default to the highest-scored
	 * {@link MoveRecord}, otherwise a random {@link Move} is chosen.
	 *
	 * @param availableMoves
	 *            a {@link List} of {@link Move Moves} available to make
	 * @param boardState
	 *            the {@link String} state of the board
	 * @return the {@link Move} to make
	 * @throws SQLException
	 *             if an error occurred while retrieving {@link MoveRecord
	 *             MoveRecords}
	 */
	public Move makeMove(ArrayList<Move> availableMoves, String boardState) throws SQLException {
		Statement statement = fCon.createStatement();
		ResultSet results = statement.executeQuery(String
				.format("SELECT * FROM moves WHERE boardState = %s AND piece = %s", boardState, fMovePiece.toString()));

		List<MoveRecord> records = new ArrayList<>();
		while (results.next())
			records.add(getMoveRecordFromResults(results));

		MoveRecord highestScoringMove = findHighestScoringMove(records);
		Move moveToMake;
		boolean usingRecord = false;
		if (highestScoringMove == null)
			moveToMake = getRandomMove(availableMoves);
		else if (getScoreForMove(highestScoringMove) <= 0) {
			List<Move> unknownMoves = new ArrayList<>();
			for (MoveRecord record : records)
				if (!availableMoves.contains(record.getMove()))
					unknownMoves.add(record.getMove());

			if (unknownMoves.size() == 0) {
				usingRecord = true;
				moveToMake = highestScoringMove.getMove();
			} else
				moveToMake = getRandomMove(unknownMoves);
		} else {
			usingRecord = true;
			moveToMake = highestScoringMove.getMove();
		}

		if (usingRecord) {
			fMovesMade.add(highestScoringMove);
			fNewMoveMap.put(highestScoringMove, false);
		} else {
			moveToMake = Move.Builder.create(moveToMake).withPiece(fMovePiece).build();
			MoveRecord newMove = MoveRecord.Builder.create().withBoardState(boardState).withMove(moveToMake).build();
			fMovesMade.add(newMove);
			fNewMoveMap.put(newMove, true);
		}
		return moveToMake;
	}

	/**
	 * Finds the highest scoring {@link MoveRecord} from a {@link List}
	 *
	 * @param records
	 *            the {@link MoveRecord MoveRecords} to search through
	 * @return the highest scoring {@link MoveRecord}, or {@code null} if no
	 *         {@link MoveRecor MoveRecords} were provided
	 */
	private MoveRecord findHighestScoringMove(List<MoveRecord> records) {
		if (records == null || records.size() == 0)
			return null;

		Iterator<MoveRecord> moveIterator = records.iterator();
		MoveRecord bestMove = moveIterator.next();
		int highestScore = getScoreForMove(bestMove);
		while (moveIterator.hasNext()) {
			MoveRecord move = moveIterator.next();
			int score = getScoreForMove(move);
			if (score > highestScore) {
				highestScore = score;
				bestMove = move;
			}
		}
		return bestMove;
	}

	/**
	 * Gets the current state of the board from a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the {@link String} board state
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private String getBoardState(ResultSet results) throws SQLException {
		return results.getString("boardState");
	}

	/**
	 * Gets the column index for a {@link Move} from a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the column for the move
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private int getColumnFromResults(ResultSet results) throws SQLException {
		return results.getInt("col");
	}

	/**
	 * Gets the total number of times a {@link Move} has resulted in a loss from
	 * a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the number of losses
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private int getLosses(ResultSet results) throws SQLException {
		return results.getInt("losses");
	}

	/**
	 * Gets a {@link Move} from a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the generated {@link Move}
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private Move getMoveFromResults(ResultSet results) throws SQLException {
		return Move.Builder.create().withRow(getRowFromResults(results)).withColumn(getColumnFromResults(results))
				.withPiece(getPieceFromResults(results)).build();
	}

	/**
	 * Gets a {@link MoveRecord} from a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the generated {@link MoveRecord}
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private MoveRecord getMoveRecordFromResults(ResultSet results) throws SQLException {
		Move move = getMoveFromResults(results);
		return MoveRecord.Builder.create().withMove(move).withBoardState(getBoardState(results))
				.withWins(getWins(results)).withTies(getTies(results)).withLosses(getLosses(results)).build();
	}

	/**
	 * Gets a {@link Move Move's} {@link Movepiece} from a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the {@link MovePiece} for the current move
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private MovePiece getPieceFromResults(ResultSet results) throws SQLException {
		return MovePiece.getMovePiece(results.getString("piece"));
	}

	/**
	 * Returns a randomly selected {@link Move}
	 *
	 * @param moves
	 *            a {@link List} of {@link Move Moves};
	 * @return the selected {@link Move}
	 */
	private Move getRandomMove(List<Move> moves) {
		Random rand = new Random();
		return moves.get(rand.nextInt(moves.size()));
	}

	/**
	 * Gets the row for a {@link Move} from a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the row's index
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private int getRowFromResults(ResultSet results) throws SQLException {
		return results.getInt("row");
	}

	/**
	 * Score's a {@link MoveRecord} based on its {@code wins}, {@code losses},
	 * and {@code ties}
	 *
	 * @param move
	 *            the {@link MoveRecord} to score
	 * @return the calculated score
	 */
	private int getScoreForMove(MoveRecord move) {
		int score = 0;
		score += move.getWins() * MoveRecordWeights.WIN.val();
		score += move.getTies() * MoveRecordWeights.TIE.val();
		score += move.getLosses() * MoveRecordWeights.LOSS.val();
		return score;
	}

	/**
	 * Gets the total number of times a {@link Move} has resulted in a tie from
	 * a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the number of ties
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private int getTies(ResultSet results) throws SQLException {
		return results.getInt("ties");
	}

	/**
	 * Gets the total number of times a {@link Move} has resulted in a win from
	 * a {@link ResultSet}
	 *
	 * @param results
	 *            the {@link ResultSet} to pull information from
	 * @return the number of wins
	 * @throws SQLException
	 *             if an error occurred retrieving data from the {@code results}
	 */
	private int getWins(ResultSet results) throws SQLException {
		return results.getInt("wins");
	}

	private void saveMove(MoveRecord move) throws SQLException {
		Statement statement = fCon.createStatement();
		String sql = "";
		if (fNewMoveMap.get(move))
			sql = String.format(
					"INSERT INTO moves " + "(boardState, wins, ties, losses, piece, col, row) "
							+ "VALUES (%s, %d, %d, %d, %s, %d, %d)",
					move.getBoardState(), move.getWins(), move.getTies(), move.getLosses(),
					move.getMove().getPiece().toString(), move.getMove().getColumn(), move.getMove().getRow())
					.toString();
		else
			sql = String
					.format("UPDATE moves SET " + "wins = %d, ties = %d, losses = %d "
							+ "WHERE boardState = %s AND piece = %s AND row = %d AND col = %d", move.getWins(),
							move.getTies(), move.getLosses(), move.getBoardState(),
							move.getMove().getPiece().toString(), move.getMove().getRow(), move.getMove().getColumn())
					.toString();

		statement.executeUpdate(sql);
	}
}
