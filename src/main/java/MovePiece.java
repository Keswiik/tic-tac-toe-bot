package main.java;

public enum MovePiece {
	O("O"), UNDEFINED(null), X("X");

	String value;

	private MovePiece(String val) {
		value = val;
	}

	public String getValue() {
		return value;
	}

	public static MovePiece getMovePiece(String val) {
		if (val.equalsIgnoreCase("x"))
			return X;
		else if (val.equalsIgnoreCase("o"))
			return O;
		else
			return UNDEFINED;
	}
}
