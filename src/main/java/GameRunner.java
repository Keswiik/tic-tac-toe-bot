package main.java;

import java.sql.SQLException;

public class GameRunner {

	public static void main(String[] args) throws SQLException {
		GameManager gameManager = new GameManager(false, 10);
		gameManager.play();
	}
}
