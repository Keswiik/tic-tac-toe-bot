package main.java;

public enum MoveRecordWeights {
	LOSS(-2), TIE(1), WIN(2);

	private final int weight;

	private MoveRecordWeights(int weight) {
		this.weight = weight;
	}

	public int val() {
		return weight;
	}
}
