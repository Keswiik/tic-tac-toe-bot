package main.java;

import java.util.ArrayList;

public class Board {
	private static int ROW_NUM = 3, COL_NUM = 3;
	private String[][] fBoard;
	private ArrayList<Move> fMoves;
	private MovePiece fWinner;

	public Board() {
		fWinner = MovePiece.UNDEFINED;
		fBoard = new String[ROW_NUM][COL_NUM];
		for (int i = 0; i < fBoard.length; i++)
			for (int j = 0; j < fBoard[i].length; j++) {
				fBoard[i][j] = "-";
				Move move = Move.Builder.create().withRow(i).withColumn(j).build();
				fMoves.add(move);
			}
	}

	public ArrayList<Move> getAvailableMoves() {
		return fMoves;
	}

	public String getBoardState() {
		String state = "";
		for (String[] arr : fBoard)
			for (String s : arr)
				state += s;

		return state;
	}

	public MovePiece getWinner() {
		if (fWinner == null)
			checkWinner();

		return fWinner;
	}

	public boolean isFinished() {
		return fMoves.size() == 0 || fWinner != null;
	}

	public boolean makeMove(Move move) {
		boolean success = false;
		if (!isFinished() && move.getPiece().getValue() != null && fMoves.contains(move)) {
			fMoves.remove(move);
			fBoard[move.getRow()][move.getColumn()] = move.getPiece().getValue();
			success = true;
		}

		checkWinner();

		return success;
	}

	@Override
	public String toString() {
		String board = "";
		int lastRow = fBoard.length - 1;
		for (int i = 0; i < fBoard.length; i++) {
			int lastCol = fBoard[i].length - 1;
			for (int j = 0; j < fBoard[i].length; j++) {
				board += " " + (fBoard[i][j].equals("-") ? " " : fBoard[i][j]) + " ";
				if (lastCol != j)
					board += "|";
			}

			if (i != lastRow) {
				board += "\n";
				for (int j = 0; j < fBoard[i].length * 3 + fBoard[i].length - 1; i++)
					board += "-";
				board += "\n";
			}
		}

		return board;
	}

	private MovePiece checkColumnWinner() {
		for (int j = 0; j < fBoard[0].length; j++) {
			String first = fBoard[0][j];
			boolean win = true;
			for (int i = 0; i < fBoard.length; i++)
				if (!fBoard[i][j].equals(first)) {
					win = false;
					break;
				}

			if (win)
				return MovePiece.getMovePiece(first);
		}

		return null;
	}

	private MovePiece checkDiagonalWinner() {
		int lStart = 0, rStart = fBoard.length - 1;
		String lFirst = fBoard[0][0], rFirst = fBoard[fBoard.length - 1][fBoard.length - 1];
		boolean lWin = true, rWin = true;
		for (int i = 0; i < fBoard.length; i++) {
			if (lWin && !fBoard[lStart + i][lStart + i].equals(lFirst))
				lWin = false;

			if (rWin && !fBoard[rStart - i][rStart - i].equals(rFirst))
				rWin = false;
		}

		if (!(lWin && rWin))
			return null;
		else
			return lWin ? MovePiece.getMovePiece(lFirst) : MovePiece.getMovePiece(rFirst);
	}

	private MovePiece checkRowWinner() {
		for (String[] arr : fBoard) {
			String first = arr[0];
			boolean win = true;
			for (int i = 1; i < arr.length; i++)
				if (!arr[i].equals(first)) {
					win = false;
					break;
				}

			if (win)
				return MovePiece.getMovePiece(first);
		}

		return null;
	}

	private void checkWinner() {
		MovePiece winRow = checkRowWinner();
		MovePiece winCol = checkColumnWinner();
		MovePiece winDiag = checkDiagonalWinner();

		if (winRow != null)
			fWinner = winRow;
		else if (winCol != null)
			fWinner = winCol;
		else if (winDiag != null)
			fWinner = winDiag;
	}
}
