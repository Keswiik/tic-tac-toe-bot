package main.java.builders;

public abstract class Builder<T> {
	private boolean isBuildingComplete = false;

	public abstract T build();

	public void setBuildingComplete() {
		verifyBuildingState();
		isBuildingComplete = true;
	}

	public void verifyBuildingState() {
		if (isBuildingComplete)
			throw new IllegalArgumentException();
	}
}
